package com.itb.diceroller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    Button rollButton;
    ImageView Dice1;
    ImageView Dice2;
    Button returnButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rollButton = findViewById(R.id.roll_button);
        Dice1 = findViewById(R.id.Dice1);
        Dice2 = findViewById(R.id.Dice2);
        returnButton = findViewById(R.id.return_button);

        final int [] ArrayDice = {R.drawable.dice_1,
                       R.drawable.dice_2,
                       R.drawable.dice_3,
                       R.drawable.dice_4,
                       R.drawable.dice_5,
                       R.drawable.dice_6};

        rollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = (int) (Math.random()*6);
                int j = (int) (Math.random()*6);
                if(ArrayDice[i] == ArrayDice[5] && ArrayDice[j] == ArrayDice[5]){
                    Toast.makeText(getApplicationContext(), "JACKPOT!", Toast.LENGTH_SHORT).show();
                }
                Dice1.setImageResource(ArrayDice[i]);
                Dice2.setImageResource(ArrayDice[j]);
                Dice1.setVisibility(View.VISIBLE);
                Dice2.setVisibility(View.VISIBLE);
            }
        });

        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dice1.setVisibility(View.INVISIBLE);
                Dice2.setVisibility(View.INVISIBLE);
            }
        });

        Dice1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = (int) (Math.random()*6);
                Dice1.setImageResource(ArrayDice[i]);
            }
        });

        Dice2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int j = (int) (Math.random()*6);
                Dice2.setImageResource(ArrayDice[j]);
            }
        });
    }
}